#!/bin/sh

set -e

cat samples/mbr.scm | bin/part-disk -c - -o testdisk1p.img
/sbin/sfdisk -l testdisk1p.img  | grep '32 100031  100000 48.8M  6 FAT16'
