#!/usr/bin/env scheme-script
;; Copyright © 2024 Vadym Kochan        !#
;; SPDX-License-Identifier: MIT

;; Tests for the common functions

(import
  (rnrs)
  (fs partitions common))

(display "Checking validate-parttable with valid partitions\n")
(assert (validate-parttable
          (make-parttable #t 0
            (list
              (make-part 0 1 3 3 0)
              (make-part 0 4 10 7 0) ))))

(display "Checking validate-parttable with overlapped partitions\n")
(assert (not (guard (e [(error? e) #f])
               (validate-parttable
                 (make-parttable #t 0
                   (list
                     (make-part 0 1 3 3 0)
                     (make-part 0 10 15 6 0)
                     (make-part 0 4 10 7 0) ))))))
