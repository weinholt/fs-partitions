# (fs partitions *)

This is an R6RS Scheme package for working for block storage device
partition tables.

## Storage device interface

These libraries need to access the storage device (or a disk image).
They expect your program to provide a `read-sector` procedure.

The `read-sector` procedure takes an LBA and returns the sector data
from the disk as a bytevector. In case there is an error it returns
the eof object or raises an exception.

LBA stands for Logical Block Address. Sectors are addressed
sequentially on the storage device and start with LBA 0. Disk geometry
is not significant; the sectors *lba* and *lba+1* need not be
physically close to each other.

Storage devices have a logical sector size which is not fixed at 512.
The requested LBA should be passed through as-is and must not be
scaled by any factor. The MBR format requires that sector 0 is at
least 512 bytes.

Plain disk images (such as those created by `dd`) lose the sector size
information. If you are reading data from such as disk image then you
should be prepared to handle variable sector sizes when computing a
file position for an LBA.

Future versions of the MBR library may take optional arguments for
using CHS (Cylinder, Head, Sector) addressing. But this would only be
useful for working with legacy disks because CHS is obsolete.

## Feature checklist

This package only provides partial support for everything you might
want to do with partition tables.

- [x] Read MBR partition tables
- [x] Read MBR extended/logical partitions
- [ ] Support CHS geometry for extended/logical partitions
- [x] Write MBR partition tables
- [ ] Write MBR extended/logical partitions
- [x] Support for large sector disks (such as 4Kn)
- [x] Read GPT partition tables
- [x] Write GPT partition tables
