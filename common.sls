;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2019 Gwen Weinholt
;; SPDX-License-Identifier: MIT
#!r6rs

;;; Common stuff for disk partitions

(library (fs partitions common)
  (export
    parttable
    make-parttable parttable?
    parttable-valid? parttable-disk-id parttable-partitions
    validate-parttable

    part make-part part?
    part-number part-start-lba part-end-lba part-total-lba part-type)
  (import
    (rnrs (6)))

(define-record-type parttable
  (fields valid?
          disk-id
          partitions))

(define-record-type part
  (fields number
          start-lba
          end-lba
          total-lba
          type))

(define (validate-parttable pt)
  (let check-next ((lst (parttable-partitions pt)))
    (when (not (null? lst))
      (let ((p1 (car lst)))
        (when (> (part-start-lba p1) (part-end-lba p1))
          (error 'validate-parttable "start-lba greater than end-lba"
                                     (part-start-lba p1) (part-end-lba p1)))
        (when (not (eqv? (part-total-lba p1)
                         (+ (- (part-end-lba p1) (part-start-lba p1))
                            1)))
          (error 'validate-parttable "total-lba does not match with start-lba & end-lba"
                                     (part-start-lba p1) (part-end-lba p1) (part-total-lba p1)))
        (for-each
          (lambda (p2)
            (when (or (and (<= (part-start-lba p1) (part-end-lba p2))
                           (>= (part-end-lba p1) (part-end-lba p2)))
                      (and (<= (part-start-lba p1) (part-start-lba p2))
                           (>= (part-end-lba p1) (part-start-lba p2))))
              (error 'validate-parttable "Partitions overlapped" p1 p2)))
          (cdr lst)))
      (check-next (cdr lst)))
    #t))
)
